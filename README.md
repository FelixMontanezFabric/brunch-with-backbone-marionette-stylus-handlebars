# Brunch with Marionette.  
This is a starter template project for web applications.

## Main Languages
* JavaScript
* [Stylus](http://learnboost.github.com/stylus/)
* [Handlebars](http://handlebarsjs.com/)

## Software included
We are using the following pieces of software

* [backbone](http://backbonejs.org/)
* [Underscore](http://underscorejs.org/)
* [bootstrap 3.1.0](http://getbootstrap.com/)
* jquery 2.1.0
* [backbone.marionette 1.6.2](https://github.com/marionettejs/backbone.marionette)
* [bootstrap-switch 2.0.1](http://www.bootstrap-switch.org/)
* [font-awesome 4.0.3](http://fortawesome.github.io/Font-Awesome)
* [Backbone.Validation 0.9.1](https://github.com/thedersen/backbone.validation)
* [jquery-serialize-object 2.0.1](https://github.com/macek/jquery-serialize-object)

Using bower to manage front end packages such as Bootstrap, FontAwesome etc.

Note that assets are not currently copied over from bower_components to public. The guys over at brunch are working on a fix for this.  When you want to update bootstrap or fontawesome: update the version in bower.json.  Then copy the font assets from bootstrap and fontawesome to app/assets/fonts folder. ( @todo, write something custom make build file that handles that ).

Using npm to manage dev packages such as stylus, handlebars. 

We are using Backbone.Router with HTML 5 pushState enabled with hashbang fallback. There is an .htaccess file that redirects all requests to the index page. 

## Getting Started


### Creating a new project 
````
brunch new git@bitbucket.org:andrewmtoy/f30.git {{project name}}
````

### Taking a look
Build the project with 
````
brunch build
````
Then open the public folder and open index.html in your browser to view the result.


## Building
To build with this project execute the following command.  This watches for changes and rebuilds.

````
brunch w
```` 

## Deployments
For deployments execute the following: 

````
brunch build --production
````

This will build the public folder and minifiy the files in there as well. 

## Legal Stuff
Some legal stuff here.

## Notes
Everything in app/assets is directly copied over to the public folder.  

For deployments, there are three paths on the App object that need to be changed.
The base href of the application must also be changed.
* App.root
* App.BaseURL
* App.API
* the base tag's href in app/index.html.
