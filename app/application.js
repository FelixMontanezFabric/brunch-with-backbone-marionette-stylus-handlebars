// this file extends backbone.validation to work with bootstrap form classes
require('lib/backbone.validation.for.bootstrap');
// this is required for marionette to run with brunch.
require('lib/marionette.brunch');
require('lib/view_helper');

var App = Backbone.Marionette.Application.extend({
  root: 'f30-skeleton/client/public',       // The root path to run the application through.
  BaseURL: 'http://localhost:8888/f30-skeleton/client/public/',        // The Base application URL
  API: 'http://localhost:8888/f30-skeleton/api/', // Base API URL (used by models & collections 

  initialize: function() {
    var _this = this;
    
    this.on('initialize:after', function(options) {
      // Backbone.history.start();
      return typeof Object.freeze === "function" ? Object.freeze(_this) : void 0;
    });
    
    this.addInitializer(function(options){
      var AppLayout = require('views/AppLayout');
      _this.layout = new AppLayout();
      _this.layout.render();
    });
    
    this.addInitializer(function(options){
      var Router = require('lib/router');
      _this.router = new Router();
    });

  }
});

module.exports = new App();
