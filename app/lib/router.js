var app = require('application');

var UserModel = require('models/user-model');
var SignUpView = require('views/sign-up-view');

var HomeView = require('views/home-view');
var UserCollection = require('models/user-collection');

var LoginView = require('views/login-view');

var BaseModalView = require('views/base-modal-view');
var SignInPromptTemplate = require('views/templates/sign-in-prompt');

var CompleteUserView = require('views/complete-user-view');


module.exports = Backbone.Router.extend({
  routes: {
    '': 'login',
    'users':'displayUsers',
    'users/:id': 'displayUser',
    'signup':'signup',
    'login': 'login',
    'go/one': 'signup'
  },
 // @todo, really need to rename these routes and methods.
  signup: function(){
    console.log('sign up');
    var userModel = new UserModel();
    var signUpView = new SignUpView({model:userModel});
    app.layout.content.show(signUpView);
  },

  displayUsers: function(){
    var userCollection = new UserCollection();
    this.userCollection = userCollection;
    var promise = userCollection.fetch({
      error: function(){
        var SignInView = new BaseModalView({template: SignInPromptTemplate});
        app.layout.modal.show(SignInView);
      }
    });
    promise.done(function(){
      var homeView = new HomeView({collection: userCollection});
      app.layout.content.show(homeView);
    });
  },

  displayUser: function(userId){
    var userModel = new UserModel({id: userId});
    var promise = userModel.fetch({
      error: function() {
        console.log('error');
      },
      success: function(model, response, options){
        if (response == false){
          console.log('no model here');
        }
      }
    });
    promise.done(function(){
      var completeUserView = new CompleteUserView({model: userModel});
      app.layout.content.show(completeUserView);
    });
  },

  login: function() {
    // show the login view
    var loginView = new LoginView();
    app.layout.content.show(loginView);
  }
});
