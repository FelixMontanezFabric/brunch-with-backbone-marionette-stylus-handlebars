var app = require('application');
module.exports = {
  login: function(e){
    e.preventDefault();
    $('.alert-error').hide();
    var url = app.API + 'login';
    var data = $('#js-login-form').serializeObject();
    $.ajax({
      url:url,
      type:'POST',
      dataType: 'json',
      data: data,
      success:function(data){
        if (data.error){
          console.log('error! you were not logged in');
        } else {
          console.log('you were logged in!');
        }
      },
    });
  },

  logout: function(e){
    e.preventDefault();
    var url = app.API + 'logout'
    $.ajax({
      url:url,
      type:'POST',
      dataType: 'json',
      success:function(data){
        console.log('you were logged out');
      },
    });
  }

}