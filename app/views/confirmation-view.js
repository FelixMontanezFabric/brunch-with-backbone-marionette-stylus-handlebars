var ConfirmationTemplate = 'views/templates/confirmation';


module.exports = Backbone.Marionette.ItemView.extend({
  // Properties
  // --------------------------------------------------------
  id: 'login-view',
  template: ConfirmationTemplate,

  // Lifecycle 
  // --------------------------------------------------------
  initialize: function(){

  },

  onRender: function(){

  },


  // Events
  // --------------------------------------------------------


  // Methods
  // --------------------------------------------------------



});
