var SignUpTemplate = require('views/templates/sign-up');
var App = require('application');
var BaseModalView = require('views/base-modal-view');
var FailureDialogTemplate = require('views/templates/failure-dialog');
var SuccessDialogTemplate = require('views/templates/success-dialog');
var UserCollection = require('models/user-collection');

module.exports = Backbone.Marionette.ItemView.extend ({
  // Lifecycle
  //-------------------------------------------------------------------------
  initialize: function(){
    // this hooks up the validation
    // note: validation must be hooked up after the model has been instantiated
    Backbone.Validation.bind(this);
  },

  onClose: function(){
    Backbone.Validation.unbind(this);
  },

  //Properties
  //-------------------------------------------------------------------------
  template: SignUpTemplate,

  // Events 
  //-------------------------------------------------------------------------
  events: {
    'submit form': 'signUp'
  },

  //Methods
  //-------------------------------------------------------------------------
  signUp: function(e){
    e.preventDefault();
    var formData = this.$el.find('form').serializeObject();
    this.model.set(formData);
    // Check if the model is valid before saving
    if(this.model.isValid(true)){
      this.model.save();

      // This is how you spawn a modal. 
      // you can make an entirely new view and pass it to modal.show
      // or you can just add another template to base-modal-view
      // and use the template by passing in an option.
      var successDialogView = new BaseModalView({template: SuccessDialogTemplate});
      App.layout.modal.show(successDialogView);
    } else {
      var failureDialogView = new BaseModalView({template: FailureDialogTemplate});
      App.layout.modal.show(failureDialogView);
    }
  }
});