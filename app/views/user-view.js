var UserTemplate = 'views/templates/user';

module.exports = Backbone.Marionette.ItemView.extend({
  // Properties
  // --------------------------------------------------------
  className: 'user',
  template: UserTemplate,

  // Lifecycle 
  // --------------------------------------------------------
  initialize: function(){
    // console.log('this', this);
  },
 });