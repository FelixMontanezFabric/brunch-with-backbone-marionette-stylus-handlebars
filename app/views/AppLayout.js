var app = require('application');
var ModalRegion = require('views/modal-region-view');

module.exports = Backbone.Marionette.Layout.extend({
  template: 'views/templates/appLayout',
  
	el: 'body',

	regions: {
    content: '#content',
    modal: ModalRegion
	}
});
