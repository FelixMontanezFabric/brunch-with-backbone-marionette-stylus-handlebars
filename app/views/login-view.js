var LoginTemplate = require('views/templates/login');
var UserSessionHelper = require('lib/user-session-helper');
module.exports = Backbone.Marionette.ItemView.extend({
  // Properties
  // --------------------------------------------------------
  id: 'login-view',
  template: LoginTemplate,

  // Lifecycle 
  // --------------------------------------------------------
  initialize: function(){

  },

  onRender: function(){
    $(this.el).find("[name='my-checkbox']").bootstrapSwitch();
  },


  // Events
  // --------------------------------------------------------
  events: {
    'submit #js-login-form': 'login',
    'click #js-log-out': 'logout'
  },

  // Methods
  // --------------------------------------------------------
  login: function(e){
    UserSessionHelper.login(e);
  },

  logout: function(e){
    UserSessionHelper.logout(e);
  }

});
