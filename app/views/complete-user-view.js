var CompleteUserTemplate = require('views/templates/complete-user');
var UserRemovedTemplate = require('views/templates/user-removed');
var BaseModalView = require('views/base-modal-view');
var App = require('application');
var SuccessDialogTemplate = require('views/templates/success-dialog');
var FailureDialogTemplate = require('views/templates/failure-dialog');

module.exports = Backbone.Marionette.ItemView.extend({
  // Properties
  // --------------------------------------------------------
  className: 'user',
  template: CompleteUserTemplate,


  // Lifecycle 
  // --------------------------------------------------------
  initialize: function(){
    Backbone.Validation.bind(this);
  },

  onClose: function(){
    Backbone.Validation.unbind(this);
  },



  // Events 
  // --------------------------------------------------------
  events: {
    "click .js-user-delete": "removeModel",
    "submit #js-edit-user-form": "editModel"

  },
  modelEvents: {
    "destroy": "modelDestroyed",
    // when the model is changed, the PUT request returns with the database id and sets it on the model
    // after the model's been rendered.  we are listening to changes so that it rerenders correctly if we 
    // want to display the id.
    "change": "render"
  },

  // Methods
  // --------------------------------------------------------
  removeModel: function(){
    this.model.destroy();
    var userRemovedView = new BaseModalView({template: UserRemovedTemplate});
    App.layout.modal.show(userRemovedView);
  },

  editModel: function(e){
    e.preventDefault();
    var formData = this.$el.find('#js-edit-user-form').serializeObject();
    this.model.set(formData);
    // Check if the model is valid before saving
    if(this.model.isValid(true)){
      this.model.save();
      // This is how you spawn a modal. 
      // you can make an entirely new view and pass it to modal.show
      // or you can just add another template to base-modal-view
      // and use the template by passing in an option.
      var successDialogView = new BaseModalView({template: SuccessDialogTemplate});
      App.layout.modal.show(successDialogView);
    } else {
      var failureDialogView = new BaseModalView({template: FailureDialogTemplate});
      App.layout.modal.show(failureDialogView);
    }
  },

  modelDestroyed: function(){
    // console.log('the model has been destroyed');
  },

  modelChanged: function(){
    // console.log('the model has been changed');
  }

 });