var UserView = require('views/user-view');
var HomeTemplate = require('views/templates/home');
var UserModel = require('models/user-model');

module.exports = Backbone.Marionette.CompositeView.extend({
  // Properties
  // --------------------------------------------------------
  id: 'home-view',
  itemView: UserView,
	template: HomeTemplate,

  // Lifecycle 
  // --------------------------------------------------------
  initialize: function(){
    console.log('home view collection view', this);
  },

  onRender:function(){
    console.log('this event occurs after rendering as taken place');
  },

  onDomRefresh: function(){
    console.log('this event occurs when the view has been loaded and rendered');
  },

  onClose: function(){
  },

  // Events
  // --------------------------------------------------------
  events: {
    'submit form': 'addUser'
  },

  modelEvents: {
    "change": "modelChanged"
  },

  collectionEvents: {
    "add": "modelAdded",
    "remove": "modelRemoved"
  },

  // Methods
  // --------------------------------------------------------
  addUser: function(e){
    e.preventDefault();
    var formData = this.$el.find('form').serializeObject();
    console.log(formData);
    var userModel = new UserModel(formData);
    userModel.save();
    this.collection.add(userModel);
  },

  modelChanged: function(){
    console.log('the collection view has detected a model has been changed');
  },

  modelAdded: function(){
    console.log('the collection view has detected a model has been added');
    console.log('this.collection', this.collection);
  },

  modelRemoved: function(){
    console.log('the collection view has detected a model has been removed');
  }
});
