module.exports = Backbone.Marionette.Region.extend({
  // this manages modal views.  calls modal hide and modal show when the view its handling 
  // shows/closes.
  // this el is in appLayout.hbs
  el: "#modal",
  constructor: function(){
    _.bindAll(this, 'getEl', 'showModal', 'hideModal');
    Backbone.Marionette.Region.prototype.constructor.apply(this, arguments);
    this.on("show", this.showModal, this);
  },

  getEl: function(selector){
    var $el = $(selector);
    $el.on("hidden", this.close);
    return $el;
  },

  showModal: function(view){
    view.on("close", this.hideModal, this);
    this.$el.modal('show');
  },

  hideModal: function(){
    this.$el.modal('hide');
  }
});