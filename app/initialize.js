var app = require('application');
var SessionModel = require('models/session-model');

$(document).ready(function() {

  app.initialize();

  app.addInitializer(function(options){

    // Create a new session model and scope it to the app global
    // This will be a singleton, which other modules can access

    this.sessionModel = new SessionModel({});

    // Check the auth status upon initialization, 
    // before rendering anything or matching routes
     app.sessionModel.checkAuth({
      // Start the backbone routing once we have captured a user's auth status
      // need to write the checkAuth function
      complete: function(){
        // HTML% pushState for URLs without hashbangs
        var hasPushState = !!(window.history && history.pushState);
        if (hasPushState) {
          Backbone.history.start({pushState: true, root: '/'});
        }
        else {
          Backbone.history.start();
        }
      }
    });

    var hasPushState = !!(window.history && history.pushState);
    if (hasPushState) {
      Backbone.history.start({pushState: true, root: app.root});
    }
    else {
      Backbone.history.start();
    }

    // Globally capture clicks.  If they are internal and not in the pass
    // through list, route them through Backbone's navigate method.
    $(document).on("click", "a[href^='/']", function(event) {
      var href, passThrough, url;
      href = $(event.currentTarget).attr('href');

      // add more black list routes with || 
      passThrough = href.indexOf('sign_out') >= 0;
      // allow shift+click for new tabs
      if (!passThrough && !event.altKey && !event.ctrlKey && !event.metaKey && !event.shiftKey) {
        event.preventDefault();
        // remove leading slashes and hash bangs (backward compatability)
        url = href.replace(/^\//, '').replace('\#\!\/', '');

        // instruct backbone to trigger routing events 
        app.router.navigate(url, {trigger: true});
        return false;
      }
    });

  });

  app.start();

});
  
  
