// Base class for all collections.
module.exports = Backbone.Collection.extend({

  // fetch: function(options){
  //   // @desc custom fetch that redirects based on server error status

  //   // if there is an error method passed in as an option
  //   // run that error method.
  //   var originalError = options.error;
  //   // overwrite the error function passed in as a function
  //   options.error = function(collection, response, options){
  //     if (originalError){
  //       // run the original error method.
  //       originalError(collection, response, options);
  //     }
  //     // then redirect based on response status.
  //     if (response.status == 401){
  //       Backbone.history.navigate('login', true);
  //     }
  //     if (response.status == 403){
  //       Backbone.history.navigate('denied', true);
  //     }
  //   };
  //   // run original fetch method.
  //   Backbone.Collection.prototype.fetch.apply(this, arguments);
  // }
});
