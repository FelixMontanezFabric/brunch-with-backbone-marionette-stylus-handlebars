var app = require('application');
var UserModel = require('models/user-model');

module.exports = Backbone.Model.extend({
  defaults: {
    loggedIn: false,
    userId: ''
  },

  initialize: function(){
    _.bindAll(this, 'updateSessionUser');

    // Singleton user object, how is this a singleton? look this up.
    // Access or listen on this through any module with app.session.user
    this.user = new UserModel({});
  },

  url: app.API + '/auth',

  updateSessionUser: function(userData){
    this.user.set( _.pick( userData, _.keys(this.user.defaults) ) );
  },

  checkAuth: function(callback, args){
    this.set({loggedIn: true});
  }



});
