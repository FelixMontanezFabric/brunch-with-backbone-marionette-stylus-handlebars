var app = require('application');
var BaseCollection = require('models/collection');
var UserModel = require('models/user-model')
module.exports = BaseCollection.extend({
  url: app.API + 'users',
  model: UserModel
});
