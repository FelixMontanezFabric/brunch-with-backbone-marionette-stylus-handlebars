// Base class for all models.
var BaseModel = require('models/model');
var app = require('application');
module.exports = BaseModel.extend({

  urlRoot: function(){
    return app.API + 'users';
  },

  // Validation 
  validation: {
    username: {
      required: true
    },
    email: {
      required:true,
      pattern: 'email'
    },
    password: {
      minLength: 8
    },
    repeatPassword: {
      equalTo: 'password',
      msg: 'The passwords do not match'
    }
  }
});
