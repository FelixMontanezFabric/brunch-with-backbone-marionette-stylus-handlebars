exports.config =
  # See docs at http://brunch.readthedocs.org/en/latest/config.html.
  files:
    javascripts:
      defaultExtension: 'coffee'
      joinTo:
        'javascripts/app.js': /^app/
        'javascripts/vendor.js': /^bower_components/
        'test/javascripts/test.js': /^test[\\/](?!vendor)/
        'test/javascripts/test-vendor.js': /^test[\\/](?=vendor)/
      order:
        before: [
          'bower_components/jquery/jquery.js'
          'bower_components/underscore/underscore.js'
          'bower_components/backbone/backbone.js'
          'bower_components/marionette/lib/backbone.marionette.js'
          'bower_components/bootstrap/dist/js/bootstrap.js'
        ]
        after: []

    stylesheets:
      defaultExtension: 'styl'
      joinTo: 'stylesheets/app.css'
      order:
        before: [
          'bower_components/bootstrap/dist/css/bootstrap.min.css'
          'bower_components/font-awesome/css/font-awesome.min.css'
        ]
        after: []

    templates:
      defaultExtension: 'hbs'
      joinTo: 'javascripts/app.js'
